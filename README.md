# Setup Infrastructure

### Etapes à suivre
<br />

- Installer Docker
- Cloner ce repo
- lancer les commandes suivants
  - allumer les conteneurs ```docker-compose -f docker-dev-compose.yml up -d```
  - éteindre les conteneurs ```docker-compose -f docker-dev-compose.yml down```

### Configurer votre keycloak
<br />

- Connectez vous avec les identifiants suivants :
    - username : admin
    - pwd : admin
- Configurer votre keycloak
    - Créer vous des royaumes (realms)
    - ajouter un rôle admin dans le royaume, suivez ce tuto [comment-faire?](https://stackoverflow.com/questions/56743109/keycloak-create-admin-user-in-a-realm/65054444#65054444)
    <img src="https://i.imgur.com/Vh31VJh.png" />
    - créer des utilsateur admin et assigner le rôle admin crée précedemment 